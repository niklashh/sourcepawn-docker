# SourcePawn-Docker

## Usage

TODO

## Building manually

To install [sourcemod](https://www.sourcemod.net/downloads.php) version 1.10, you can use

```shell
docker build -t niklashh/sourcepawn . \
--build-arg SOURCEMOD_URL="https://sm.alliedmods.net/smdrop/1.10/sourcemod-1.10.0-git6492-linux.tar.gz"
```
