FROM debian:buster-slim
LABEL maintainer="kahvikannu@gmail.com"

ARG SOURCEMOD_URL="https://sm.alliedmods.net/smdrop/1.10/sourcemod-1.10.0-git6492-linux.tar.gz"
ARG SOURCEMOD_BRANCH=stable

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends --no-install-suggests \
        ca-certificates \
        libxml2-utils \
        wget \
        lib32gcc1 \
        lib32stdc++6 \
    ; \
    apt-get clean autoclean; \
    apt-get autoremove -y; \
    rm -rf /var/lib/apt/lists/*; \
    \
    useradd --create-home --shell /bin/bash steam

WORKDIR /home/steam

USER steam

RUN [ -z "$SOURCEMOD_URL" ] && SOURCEMOD_URL=`wget -qO- "https://sm.alliedmods.net/downloads.php?branch=$SOURCEMOD_BRANCH" | xmllint --html --xpath 'string(//a/*[text()="Linux"]/../@href)' -`; \
 echo "Downloading newest sourcemod ($SOURCEMOD_BRANCH) from url: $SOURCEMOD_URL"; \
 wget -qO- $SOURCEMOD_URL | tar xvzC .
